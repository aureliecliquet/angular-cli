import { MonPremierProjetPage } from './app.po';

describe('mon-premier-projet App', function() {
  let page: MonPremierProjetPage;

  beforeEach(() => {
    page = new MonPremierProjetPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
